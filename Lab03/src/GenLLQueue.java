
//linked list queue

public class GenLLQueue <t>{
	private class ListNode{
		//instance of ListNode
		private t data;
		private ListNode link;
	}
	//instance of List
	private ListNode head;
	private ListNode tail;
	//default
	public GenLLQueue(){
		this.head = null;
		this.tail = null;
	}
	
	/*
	 * Other Methods
	 */
	
	//add
	public void enqueue(t aData){
		ListNode nodeToAdd = new ListNode();
		nodeToAdd.data = aData; //set data of new node to user input data
		if(head == null){ //empty queue
			head = tail = nodeToAdd;
			return;
		}
		tail.link = nodeToAdd;
		tail = tail.link;
	}
	
	//remove 
	public t dequeue(){
		if (head == null){ //empty
			System.out.println("Empty Queue");
			return null;
		}
		t retData = head.data;
		head = head.link;
		return retData;
	}
	
	//peek into queue
	public t peek(){
		return head.data; //data at head of list 
	}
	
	//print to console
	public void showQueue(){
		ListNode position = head;
		System.out.print("\n");
		while(position != null){
			System.out.print(position.data+"\t");
			position = position.link;
		}
		System.out.println("\n");
	}
}
