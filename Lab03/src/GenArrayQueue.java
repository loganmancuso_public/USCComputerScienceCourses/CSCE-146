//array queue 

public class GenArrayQueue <t>{

	public static final int SIZE = 100; //default array size
	
	private t[] queue;
	private int tailIndex;
	
	public GenArrayQueue(){
		queue = (t[]) new Object [SIZE]; //intitalize array of size
		this.tailIndex = 0;
	}
	public GenArrayQueue(int size){
		queue = (t[]) new Object [size]; //pass user given size into new array
		this.tailIndex = 0; 
	}
	
	/*
	 * Other methods
	 */
	
	//add
	public void enqueue (t data){
		if (tailIndex >= queue.length){ //check if full or in odd case more than size of array
			System.out.println("Queue full");
			return;
		}else{ //add to first null at end 
			queue[tailIndex] = data; //store user input data to last in queue
			tailIndex ++; //move tail to one right 
		}
	}
	//remove 
	public t dequeue(){
		t retData = queue[0]; //always first index using random access
		for (int i=0; i<queue.length-1; i++){
			queue[i] = queue[i+1]; //shift all to left moving up in queue
		}
		queue[queue.length-1]=null; //set last index to null
		tailIndex --;
		return retData;
	}
	
	//peek, see the list with no changes 
	public t peek(){
		return queue[0]; //see first in line
	}
	
	//print to console
	public void showQueue(){
		for(t node : queue){
			if (node == null){
				break;
			}else{
				System.out.println(node.toString());
			}
		}
	}
}
