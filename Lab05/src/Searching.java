/**
 * 
 */

/**
 * @author Logan
 *
 */


public class Searching {
	public static int linCnt = 0;
	public static int binCnt = 0;
	public static int finLinCnt = 0;
	public static int finBinCnt = 0;
	
	//recursive search 
		public static boolean linSearRecur(int[] a, int val, int index){
			if(a[index] == val){ //if index is the toFind value 
				linCnt++; //add to counter
				return true;
			}else{
				index++;
				if (index > a.length-1){ //if not in list 
					return false;
				}else{
					linCnt++; //add to counter
					linSearRecur(a,val,index); 
				}
			}return false;
		} 
		
	//recursive binary search
	public static boolean binSearRecur(int[] a, int val, int minIndex, int maxIndex){
		int middle = (maxIndex + minIndex)/2;
		if(minIndex > maxIndex){ //not in list
			return false;
		}
		if(a[middle]== val){//if middle is chosen val
			return true;		
		}else if(val>a[middle]){ //search top half of sorted array
			binCnt ++;
			return binSearRecur(a,val,middle+1,maxIndex);
		}else if(val<a[middle]){ //search bottom half of sorted array
			binCnt ++;
			return binSearRecur(a,val,minIndex,middle-1);
		}
		return false;
	}
	public static void sortArray(int[] a){ //sorting the array using selection 
		int i, j, minIndex, tmp;
		int n = a.length;
		for(i=0; i<n-1; i++){
			minIndex = i;
			for(j=i+1; j<n; j++){
				if(a[j]<a[minIndex]){ 
					minIndex = j; 
				}
				if(minIndex != i){ //swap
					tmp = a[i];
					a[i] = a[minIndex];
					a[minIndex] = tmp;
				}
			}//end j for loop 
		}//end i for loop
	}

	/*
	 * main method
	 */
	
	public static void main(String[] args){
		int count = 0; //set number of loops
		while (count<20){
			int[] a = new int[1000];
			int toFind;
			for (int j=0; j<a.length; j++){
				a[j] = (int)(Math.random()*(999)); //construct random array of 0-999
			}
			toFind = (int)(Math.random()*(999));
			/*
			 * if array is left unsorted the recursive binary will not work properly
			 * it will disregard parts of array that may contain the value 
			 */
			sortArray(a); 
			System.out.println("\nNumber to Find: "+toFind);
			linSearRecur(a,toFind,0);
			binSearRecur(a,toFind,0,a.length);
			System.out.println("Search Using Linear Recursive: "+linCnt+
								"\nSearch Using Binary Recursive: "+binCnt);
			finLinCnt+=linCnt;
			finBinCnt+=binCnt;
			binCnt=linCnt = 0;
			count ++;
		}
		System.out.println("\nAverage:\nLinear: "+finLinCnt/count+"\nBinary: "+finBinCnt/count);
	}//end main
}
