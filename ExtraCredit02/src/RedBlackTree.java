import java.awt.Color;
import java.util.LinkedList;
import java.util.Queue;

/**
 * contains methods 
 * if node red both children are black 
 * TODO: balancing tree
 * TODO: recoloring 
 * TODO: main method 
 *  TODO: relationships, child left, child right, parent, root, uncle, grandparent 
 * insert method and coloring the new node 
 * TODO: delete method 
 * TODO: search 
 * TODO: min 
 * TODO: max
 * TODO: depth of entire tree
 * TODO: depth of entered value //condition if entered is either min or max call absolute depth method
 */

/**
 * @author Logan
 *
 */
public class RedBlackTree<Type extends Comparable<Type>> {
	/*
	 * constructing the linked binary tree and attributes of it data in node,
	 * left &/|| right child and a color: red || black
	 */
	private class Node {
		private Type data;
		private Node left, right, parent;
		private Color color;

		public Node(Type theData, Color theColor) {
			this.data = theData;
			this.color = theColor;
			this.left = null;
			this.right = null;
			this.parent = null;
		}
	}

	private Node root;

	public RedBlackTree() {
		root = null;
	}

	// insert method
	public void insert(Type data) {
		Node newNode = new Node(data, Color.BLACK);// construct new black root
		if (root == null)
			root = newNode;
		else
			toInsert(root, data, root); // call helper method
	}

	// to insert helper method
	private Node toInsert(Node aNode, Type data, Node parent) {
		if (aNode == null) { // if found
			Node nodeToAdd = new Node(data, Color.RED);
			aNode = nodeToAdd;
			aNode.parent = parent;
			colorFlip(aNode);
			leftRotation(aNode);
//			rightRotation(aNode);
//			 System.out.println("Parent of " + aNode.data + " is " + aNode.parent.data);
		} else if ((data.compareTo(aNode.data) < 0)) { // go left
			aNode.left = toInsert(aNode.left, data, aNode); // Recursively call
		} else if ((data.compareTo(aNode.data) >= 0)) { // go right
			aNode.right = toInsert(aNode.right, data, aNode); // Recursively
																// call
		}
		return aNode;
	}

	private void colorFlip(RedBlackTree<Type>.Node aNode) {
		// TODO Auto-generated method stub
		if (aNode.left == null || aNode.right == null)
			return;
		if (!isRed(aNode) && isRed(aNode.right) && isRed(aNode.left)) {
			if (aNode != root) {
				aNode.color = Color.RED;
			}
			aNode.right.color = Color.BLACK;
			aNode.left.color = Color.BLACK;
		}
	}

	private RedBlackTree<Type>.Node leftRotation(RedBlackTree<Type>.Node aNode) {
		Node currNode = aNode.left; 
		aNode.left = currNode.right;
		currNode.right = aNode;
		return currNode;	
	}

	private RedBlackTree<Type>.Node rightRotation(RedBlackTree<Type>.Node aNode) {
		Node currNode = aNode.right;
		aNode.right = currNode.left;
		currNode.left = aNode;
		return currNode;
	}

	// delete method
	public void delete(Type data) {
		root = toDelete(root, data);
	}

	// to delete helper method
	private RedBlackTree<Type>.Node toDelete(RedBlackTree<Type>.Node n, Type data) {
		// TODO Auto-generated method stub
		if (root == null)
			return null;

		else if (n.left != null && n.right != null) {
			// has two children
		}
		return null;
	}

	/*
	 * color will be used to balance tree and using the isRed or isBlack methods
	 * will help with reorganizing
	 */
	// if that node is red, will return true or false
	private boolean isRed(Node n) {
		if (n.color == Color.RED)
			return true;
		return false;
	}

	// if that node is black, will return true or false
	private boolean isBlack(Node n) {
		if (n.color == Color.BLACK)
			return true;
		return false;
	}

	public void print() {
		printBreadthOrder(root);
	}

	private void printBreadthOrder(Node aNode) {
		Queue<Node> queue = new LinkedList<Node>();
		if (aNode == null)
			return;
		queue.add(aNode);
		while (!queue.isEmpty()) {
			Node n = (Node) queue.remove();
			System.out.println(" " + n.data);
			if (n.left != null)
				queue.add(n.left);
			if (n.right != null)
				queue.add(n.right);
		}
	}

	// parent node method
	public RedBlackTree<Type>.Node parent(Type target) {
		return parentHelper(target, root, null);
	}

	// parent helper
	private RedBlackTree<Type>.Node parentHelper(Type target, Node aNode, Node parent) {
		// TODO Auto-generated method stub
		if (aNode == null)
			return null;
		else if (!aNode.data.equals(target))
			parent = parentHelper(target, aNode.left, aNode);
		if (parent == null)
			parent = parentHelper(target, aNode.right, aNode);
		return parent;
	}

}
