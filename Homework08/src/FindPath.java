public class FindPath {
	
	/*****************************************************
	 * Original Structure 
	 * 
	 *	            (A) 
	 *   _|1|_|2|_       __|4|__
	 *  /         \_____/       \
	 * |    (B)    __3__   (D)   |
	 *  \_   _   _/	    \__   __/
	 *    |5| |6|          |7|
	 *              (C)
	 * 
	 *****************************************************/
	
	public static final int BRIDGE_WEIGHT=10;//they all have the same weight 
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Welcome to the Konigsberg Bridge Puzzle\n");//prompt the user
		KonigsbergBridgePuzzle puzzle= new KonigsbergBridgePuzzle ();//create the graph 
		//Create the four vertexes
		puzzle.addVertex("Land 1");//a
		puzzle.addVertex("Land 2");//b
		puzzle.addVertex("Land 3");//c
		puzzle.addVertex("Land 4");//d 
		puzzle.addEdge ("Land 1", "Land 2", BRIDGE_WEIGHT);//Bridge 1
		puzzle.addEdge ("Land 2", "Land 3", BRIDGE_WEIGHT);//Bridge 5 
		puzzle.addEdge ("Land 1", "Land 2", BRIDGE_WEIGHT);//Bridge 2
		puzzle.addEdge ("Land 4", "Land 2", BRIDGE_WEIGHT);//Bridge 3
		puzzle.addEdge ("Land 4", "Land 3", BRIDGE_WEIGHT);//Bridge 7
		puzzle.addEdge ("Land 4", "Land 1", BRIDGE_WEIGHT);//Bridge 4
		puzzle.addEdge ("Land 1", "Land 4", BRIDGE_WEIGHT);//Bridge 4
		puzzle.addEdge ("Land 2", "Land 1", BRIDGE_WEIGHT);//Bridge 1
		puzzle.addEdge ("Land 2", "Land 1", BRIDGE_WEIGHT);//Bridge 2
		puzzle.addEdge ("Land 3", "Land 2", BRIDGE_WEIGHT);//Bridge 6
		puzzle.addEdge ("Land 3", "Land 4", BRIDGE_WEIGHT);//Bridge 7
		puzzle.addEdge ("Land 3", "Land 2", BRIDGE_WEIGHT);//Bridge 5
		int count=0;//use this to determine if there is a Euler path 
		int land1Count= puzzle.checkForOddDegree("Land 1");//it will return a one if is has an odd degree and 0 
		//if it has an even degree
		int land2Count= puzzle.checkForOddDegree("Land 2");
		int land3Count= puzzle.checkForOddDegree("Land 3");
		int land4Count= puzzle.checkForOddDegree("Land 4");
		count= land1Count + land2Count + land3Count + land4Count;
		if(count == 4)//all of the vertices have odd degrees- since they do, there is not an Euler path 
		{
			System.out.println("No Path Found Too Many Odd Nodes");
		}
		else
		{
			System.out.println("Path Found");
		}

	}

}
