
//logan mancuso 
/*
 * create circle linked list
 * insert method
 * add player to list
 * each competition between players 
 */

public class rpsDatabase {

	player aPlayer = new player();
	
	private class ListNode{ //internal class to construct queue linked list
		
		//instance variables 
		private player playerInfo; //player info is the data within the list
		private ListNode link; 
	
		//default
		public ListNode(){
			this.playerInfo = aPlayer;
		}
	}//end private class
	
	//instance of List
	private ListNode head;
	private ListNode tail;
	//default
	public rpsDatabase(){
		this.head = null;
		this.tail = null;
	}
	
	/*
	 * Other methods
	 */

	//add player to list
	public void enqueue(String name){
		ListNode nodeToAdd = new ListNode();
		nodeToAdd.playerInfo.setName(name); //set data of new node to user input name
		if(head == null){ //empty queue
			tail = head = nodeToAdd;
			return;
		}
		tail.link = tail = nodeToAdd;
	}
	//custom to string method
	public void toString(ListNode position){
		while(position != null){
			System.out.print("Name: "+position.playerInfo.getName()+"\t"
					+ "Win: "+position.playerInfo.getWins()+"\t"
					+ "Loss: "+position.playerInfo.getLosses()+"\t"
					+ "Ties: "+position.playerInfo.getTies());
			position = position.link;
			System.out.println("\n");
		}
	}
	//print to console
	public void showQueue(){
		ListNode position = head;
		System.out.print("\n");
		this.toString(position); //call toString method for printing 
		System.out.println("\n");
	}
	//TODO: this is probably wrong below here 
	//setting current player
	public player setCurrent(){ 
		if(head == null){
			System.out.println("Empty Queue");
			return null;
		}
		player currentPlayer = head.playerInfo;
		head = head.link;
		return currentPlayer;
	}
	//TODO:not really sure 
	public void next() {
		head.link = head.link.link; //point to the link of the next link 
	}

}
