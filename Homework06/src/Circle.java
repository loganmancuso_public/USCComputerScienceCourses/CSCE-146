/**
 * 
 */

/**
 * @author Logan
 *
 */
public class Circle implements Shape{

	public double radii;
	
	public Circle(){
		this.radii = 0.0; 
	}
	public Circle(double radii){
		this.setRadii(radii);
	}
	public double getRadii() {
		return radii;
	}
	public void setRadii(double radii) {
		if(radii > 0)
			this.radii = radii;
		else 
			System.out.println("Error in setting Radii");
	}
	
	public double getArea(){
		return Math.round(Math.PI*(Math.pow(getRadii(), 2))); //Pi*r^2
	}
	public double getPerimeter(){
		return Math.round(2*Math.PI*this.getRadii()); //2*Pi*r
	}
	public String getType(){
		return "Circle";
	}
	public void print(){ 
		System.out.println("Circle:\t\tRadius: "+this.getRadii()+"\t\t\tArea: "+this.getArea()+"\tPerimeter: "+this.getPerimeter());
	}
}
