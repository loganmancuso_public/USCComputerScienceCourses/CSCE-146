/**
 * 
 */

/**
 * @author Logan
 *
 */
public class Rectangle implements Shape{

	public double length,width;
	
	public Rectangle(){
		this.length = 0.0;
		this.width = 0.0; 
	}
	public Rectangle(double lenght,double width){
		this.setLength(length);
		this.setWidth(width);
	}
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		if(length > 0)
			this.length = length;
		else
			System.out.println("Error in setting Lenght for Rectangle");
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		if(width > 0)
			this.width = width;
		else 
			System.out.println("Error in setting Width for Rectangle");
	}
	public double getArea(){
		return this.getLength()*this.getWidth(); //l*w
	}
	public double getPerimeter(){
		return 2*(this.getLength()+this.getWidth()); //2*(l+w)
	}
	public String getType(){
		return "Rectangle";
	}
	public void print(){
		System.out.println("Rectangle:\tLength: "+this.getLength()+"\tWidth: "+this.getWidth()+"\tArea: "+this.getArea()+"\tPerimeter: "+this.getPerimeter());
	}
}
