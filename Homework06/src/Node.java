/**
 * 
 */

/**
 * @author Logan
 *
 */
public class Node {
		
	public Shape data; 
	public Node left,right;
	
	public Node(){
		this.data = null;
		this.left = null;
		this.right = null; 
	}
	public Shape getData() {
		return data;
	}
	public void setData(Shape data) {
		this.data = data;
	}
	public Node getLeft() {
		return left;
	}
	public void setLeft(Node left) {
		this.left = left;
	}
	public Node getRight() {
		return right;
	}
	public void setRight(Node right) {
		this.right = right;
	}
}
