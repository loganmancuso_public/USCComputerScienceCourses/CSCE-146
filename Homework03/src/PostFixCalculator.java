/**
 * 
 */

/**
 * @author Logan
 *
 */
import java.util.Scanner;
import java.util.StringTokenizer;


public class PostFixCalculator {

	/**
	 * @param args
	 * @throws DivideByZeroExcep 
	 * @throws TooManyOperandExcep 
	 * @throws TooFewOperandExcep 
	 */
	
	public static void main(String[] args) throws TooFewOperandExcep, TooManyOperandExcep, DivideByZeroExcep {
		// TODO Auto-generated method stub
		Scanner kybd = new Scanner(System.in);
		boolean flag = true;
		try{
			System.out.println("This is a post fix calculator:"
					+ "\nEnter equation in Post Fix Notation (with white space)"
					+ "\nOr quit");
			String input = kybd.nextLine();
			/*
			 * this is a really messy way of stopping the loop
			 * but for some reason I tried to set 
			 * while input != "quit" but i'm not sure why it doesn't work 
			 * probably same reason the op1 == '0' in line 126 doesn't work 
			 * 		(divide by zero exception case) is line 126 
			 */
			if (input.equalsIgnoreCase("quit")){
				flag = false;
			}
			while(flag){//loop while not "quit" 
				try{ //try, catch: for input of user string equation 
					if (input.length() <= 1){ //if one number
						input = null;
						throw new TooFewOperandExcep();
					}
				}catch (Exception e){
					System.out.println(e.getMessage());
				}System.out.println("Answer: "+calculation(input)+"\n");
				System.out.println("\nEnter equation in Post Fix Notation (with white space)"
						+ "\nOr quit");
				input = kybd.nextLine();
				if (input.equalsIgnoreCase("quit")){
					flag = false;
				}
			}//end while
		}catch(InvalidOpExcep ioe){
			System.out.println(ioe.getMessage());
		}catch(TooFewOperandExcep ioe){
			System.out.println(ioe.getMessage());
		}catch(TooManyOperandExcep ioe){
			System.out.println(ioe.getMessage());
		}catch(DivideByZeroExcep ioe){
			System.out.println(ioe.getMessage());
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}//end main 
		
	/**
	 *Calculations to perform with switch case of each +,-,/,*
	 */
	
	//calculation 
	public static int calculation(String operation) throws 
						InvalidOpExcep, 
						TooFewOperandExcep, 
						TooManyOperandExcep, 
						DivideByZeroExcep
	{
		Stack<Integer> equation = new Stack<Integer>();  //new stack class  
		StringTokenizer strgTkn = new StringTokenizer(operation); //new tokenizer to break up input string
		String tkn; //token 
		int opnd; //operand
		char currChar; //current character 
		while(strgTkn.hasMoreTokens()){
			tkn = strgTkn.nextToken(); //setting to each new token
			if (tkn.length() > 1){ //if a non single digit 
				try{
				//	System.out.println("non single digit"); //error check
					opnd = Integer.parseInt(tkn);
					equation.push(new Integer(opnd));
				}catch(NumberFormatException nfe){
					throw new InvalidOpExcep();
				}
			}else{ //single digit
				//System.out.println("single digit"); //error check
				currChar = tkn.charAt(0);
				if(Character.isDigit(currChar)){
				//	System.out.println("Here"); //error check
					equation.push(Character.digit(currChar, 10));
				}else if(Character.isLowerCase(currChar)){ //if a letter
					System.out.println("variables not accepted");
				}else{ //operation
					try{
						System.out.println("\nBefore: "); equation.showStack(); //error check
						Object numbTwo = equation.pop(); //FILO so 2 is first off stack
						Object numbOne = equation.pop(); 
						System.out.println("\nAfter: "); equation.showStack(); //error check
					//	System.out.println(numbOne+" "+numbTwo+" "+rest+" "+ currChar); //error check
						int op2 = ((Integer)numbTwo).intValue(); //setting corresponding integer values from poped values
						int op1 = ((Integer)numbOne).intValue();
						System.out.println("\nEquation: "+op2+" "+currChar+" "+op1+" ="); //error check
					//	System.out.println("Here, too"); //error check
						switch (currChar){
							case '+': //add
								equation.push((op1+op2));
								break;
							case '-': //subtract
								equation.push((op1-op2));
								break;
							case '*': //multiply
								equation.push((op1*op2));
								break;
							case '/': //divide
								if (op1 == '0'){//if denominator = zero 	
									throw new DivideByZeroExcep();
								}else{ //else denominator !=0
									equation.push((op2/op1));
								}
								break;
							case ' ':
								throw new TooFewOperandExcep();
							default: //if else operand used  
								throw new InvalidOpExcep();
						}//end switch case
					}catch(TooFewOperandExcep tfoe){
						tfoe.getLocalizedMessage();
					}catch(InvalidOpExcep ioe){
						ioe.getLocalizedMessage();
					}catch(DivideByZeroExcep dbze){
						dbze.getLocalizedMessage();
					}
				}//end else statement 
			}
		}//end while
		/*
		 * return new string of equation 
		 */
		if (equation.headIndex == 0){  //to check if stack has only one numb left   
			int result = equation.pop();  
			return result;
		}else if (equation.headIndex>0){
		//	System.out.println("here"); //error check remove after 
			calculation(equation.toString()); //call recursively to complete the equation 
		}else{
			System.out.println("error"); 
		}
		return 0;
	}
} //end class
	
