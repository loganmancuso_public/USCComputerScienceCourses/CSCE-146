/**
 * 
 */

/**
 * @author Logan
 *
 */
public class DivideByZeroExcep extends Exception {
	//default
	public DivideByZeroExcep(){
		super("Invalid operation divide by zero");
	}
	//param for err msg
	public DivideByZeroExcep(String aMsg){
		super(aMsg);
	}
}