/**
 * 
 */

/**
 * @author Logan
 *
 */
public class HeapTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("\'F\' Word Min Heap Tester\nPopulating with words:");
		F_WordHeap heap = new F_WordHeap(20); //size 20 
		//insert using for each from predetermined string array called 'array' 
		String array[] = {"FUN","Fluffy","Fife", 
				"Five Dollar Foot Long For Five Dollars", 
				"FfffFFffuuu!", "Farmer Fred Fiddles for Food",
				"After Friday fear the final. Be afraid friend."};
		
		for (String word : array){
			System.out.println(word); //print durring insert 
			heap.insert(word);
		}
		System.out.println("\n------------Printing Breadth Heap:------------"); //breadth order
		heap.printBreadth();
		System.out.println("\n--------------Testing Heap Sort:--------------"); //print the sorted heap 
		heap.heapSort();
	}
}
