/**
 * 
 */

/**
 * @author Logan
 *
 */
public class IntBSTree {

	private static final int DEF_SIZE = 128; //size base 2 tree 
	private int array[];
	public IntBSTree(){
		array = new int[DEF_SIZE];
		for(int i=0; i<array.length ; i++){
			array[i] = -1;	
		}
	}
	/**
	 * @param data
	 */
	public void insert(int data) {
		// TODO Auto-generated method stub
		if(array[0] == -1)//empty 
			array[0] = data;
		else 
			toInsert(0, data); //find place 
	}
	/**
	 * @param i
	 * @param data
	 */
	//insert helper method
	private void toInsert(int i, int data) {
		// TODO Auto-generated method stub
	//	System.out.println("Test:" + data);
		if(array[i] == -1) //found empty, place at index i   
			array[i] = data;
		else if(data < array[i]){ //left
			if (i*2+1 < array.length)
				toInsert(i*2+1 ,data);
		}else if(data >= array[i]){ //right
			if(i*2+2  < array.length)
				toInsert(i*2+2 ,data);
		}
	}
	/**
	 * @param data
	 * @return
	 */
	public int getDepth(int data) {
		// TODO Auto-generated method stub
		return depth(0,data);
	}
	/**
	 * @param i
	 * @param data
	 * @return
	 */
	//get depth helper method 
	public int depth = 0; //Reference to update depth counter outside of recursive call
	private int depth(int i, int data) {
		// TODO Auto-generated method stub
		if(i==-1) //found 
			return -1;
		else if(data == array[i])
			return depth;
		else if(array[i]-data>0){ //left 
			depth++; //update depth counter
			if(i*2+1 < array.length) //index out of bounds 
				return depth(i*2+1,data);
		}else{ //right
			depth++; //update depth counter
			if(i*2+2 < array.length) //index out of bounds 
				return depth(i*2+2,data); 
		}return -1; //if not in tree return -1 
	}
	/*
	 * printing methods 
	 */
	//In Order Print method
	public void printInorder() {
		// TODO Auto-generated method stub
		print(0);
	}
	/**
	 * @param i
	 */
	private void print(int i) {
		// TODO Auto-generated method stub
		if(array[i]==-1)
			return;
		if(array[i*2+1] != -1)  //move left
			if(i*2+1 < array.length) //index out of bounds 
				print(i*2+1);
		System.out.println(array[i]);
		if(array[i*2+2] != -1) //right
			if(i*2+2 < array.length) //index out of bounds 
				print(i*2+2);
		return;
	}
	// Breadth Order Print method 
	public void printBreadthOrder() {
		// TODO Auto-generated method stub
		for(int val: array){
			if(val != -1)
				System.out.println(val); 
		}
	}	
}
