/**
 * 
 */

/**
 * @author Logan
 *
 */

import java.util.Random;
import java.io.*;
import java.util.Scanner;

public class TicTacToeDatabase {
	
	
	enum Spaces {Empty,xPiece,oPiece,}; //enumerations of each piece 
	public static final int BOARD_SIZE = 3; //size of board 3x3
	
	Spaces[][] board = new Spaces[BOARD_SIZE][BOARD_SIZE]; //2D array of board 3x3
	
	//add person to top ten score database 
	public static final String delim = "\t";
	private HighScore[] scores; //create array of names and scores 
	public TicTacToeDatabase(){ 
		scores= new HighScore[10]; //up to ten scores can be recorded
	}
	public HighScore[] getScore(){
		return this.scores;
	}
	// add the name to the score array
	public void addName(HighScore aHighScore) {
		for (int i = 0; i < 10; i++) {
			if (scores[i] == null) {
				scores[i] = aHighScore;
				break;
			}
		}
	}
	//initialize board 
	public void createBoard(){
		for (int i=0; i<BOARD_SIZE; i++){
			for (int j=0; j<BOARD_SIZE;j++){
				board[i][j] = Spaces.Empty;
			}
		}
	}
	//place players move and check if space is empty 
	public void addPlayerMove(){
		boolean spaceIsTaken = true;
		do{
			System.out.println("Enter coordinates (x,y) with a space"
					+ "\nor a negative to exit");
			Scanner kybd = new Scanner(System.in);
			int Y = kybd.nextInt(); //y coordinate 
			int X = kybd.nextInt(); //x coordinate 
			if (X<0||Y<0){ //if user enters negative 
				System.out.println("You have quit the tournimate"
						+ "\nNo score will be recorded");
				spaceIsTaken=false;
				System.exit(0); //exit prgm 
			}
			for (int i=0; i<BOARD_SIZE; i++){
				if (X == i){
					for (int j=0; j<BOARD_SIZE; j++){
						if (Y == j){
							if (board [i][j] == Spaces.Empty){ //only place if coordinate corresponds to empty space else... 
								board[i][j] = Spaces.xPiece;
								spaceIsTaken = false;
							}
							else{ //if user tries to place in a non empty space 
								System.out.println("Space is already occupied");
							}
						}
					}
				}
			}
		}while(spaceIsTaken == true); //stop loop when user chooses valid place
	}
	
	//place computers move 
	public void addCompMove(){
		Random r = new Random();
		System.out.println();
		System.out.println("CPU's Turn"); 
		boolean spaceIsTaken = true;
		do{
			int x = r.nextInt(BOARD_SIZE);
			int y = r.nextInt(BOARD_SIZE);
			for (int i=0; i<BOARD_SIZE; i++){
				if (x == i){
					for (int j=0; j<BOARD_SIZE; j++){
						if (y == j){
							if (board [i][j] == Spaces.Empty){ //same as user condition cpu must choose empty space else... 
								board[i][j] = Spaces.oPiece; 
								spaceIsTaken = false;
							}
							else { //if cpu chooses non empty space force a redo
							//	System.out.println("Ran");//comment this out from error checking 
							}
						}
					}
				}
			}
		}while(spaceIsTaken == true); //break when valid space chosen 
	}
	
	//print the board to console
	public void printBoard(){
		System.out.println(" ");
		System.out.print("_|_0_|_1_|_2_|\n");
		for (int i=0; i<BOARD_SIZE; i++){
			System.out.print(i);
			for (int j=0; j<BOARD_SIZE; j++){
				
				switch (board[i][j]){
				case Empty:
					System.out.print("|___"); //blank
					break;
				case xPiece:
					System.out.print("|_X_"); //x piece, user 
					break;
				case oPiece:
					System.out.print("|_O_"); //o piece, cpu
					break;
				default:
					System.out.print("|?"); //if else 
					break;
				}
			}
			System.out.println("|"); //edge of board 
		}
	}

	
	//conditions for game over 
	public boolean[] gameOver(boolean game, boolean addPoint){
		int cat = 0;
		for (int i=0; i<BOARD_SIZE; i++){
			for (int j=0; j<BOARD_SIZE;j++){
				//Conditions for user winning 
				if 
				//Vertical
					((board[0][i].equals(Spaces.xPiece)&&
					board[1][i].equals(Spaces.xPiece)&&
					board[2][i].equals(Spaces.xPiece))
				//Horizontal 	
					||(board[j][0].equals(Spaces.xPiece)&&
					board[j][1].equals(Spaces.xPiece)&&
					board[j][2].equals(Spaces.xPiece))
				//left right Diagonal
					||((board[0][0].equals(Spaces.xPiece)&&
					board[1][1].equals(Spaces.xPiece)&&
					(board[2][2].equals(Spaces.xPiece))))
				//right left Diagonal 
					|| (board[0][2].equals(Spaces.xPiece)&&
					board[1][1].equals(Spaces.xPiece)&&
					board[2][0].equals(Spaces.xPiece)))
					{
						this.printBoard();
						this.createBoard();
						System.out.println("\n!!!!You Win!!!!\n"
								+ "Next Game Starting");
						//TODO: make score counter here 
						addPoint=true;
						if (addPoint==true){
							System.out.println("!!!! here ");
						}
						//break;
						//stop checking 
				}
				//Conditions for CPU winning 
				else if
				//Vertical
					((board[0][i].equals(Spaces.oPiece)&&
					board[1][i].equals(Spaces.oPiece)&&
					board[2][i].equals(Spaces.oPiece))
				//Horizontal 	
					||(board[j][0].equals(Spaces.oPiece)&&
					board[j][1].equals(Spaces.oPiece)&&
					board[j][2].equals(Spaces.oPiece))
				//left right Diagonal
					||((board[0][0].equals(Spaces.oPiece)&&
					board[1][1].equals(Spaces.oPiece)&&
					(board[2][2].equals(Spaces.oPiece))))
				//right left Diagonal 
					|| (board[0][2].equals(Spaces.oPiece)&&
					board[1][1].equals(Spaces.oPiece)&&
					board[2][0].equals(Spaces.oPiece)))
					{
						this.printBoard();
						System.out.println("\nYou Lost"
								+ "\nComputer Wins");				
						game = false;
						//stop checking 
				}
				//Conditions for CAT Game
				else if
					(board[i][j].equals(Spaces.xPiece) || board[i][j].equals(Spaces.oPiece)){
					cat++; //counts number of each piece on board  
				}
				if(cat==9){ //if total places add up to nine and conditions above not met cat game is called and rematch initiated 
					this.printBoard();
					this.createBoard();
					System.out.println("\nCAT Game\n"
							+ "Next Game Starting");	
					break; 
				}
			}
		
		}	
		if (addPoint==true){
			System.out.println("!!!! still here ");
		}
		return new boolean[] {addPoint,game}; 
	}
	
	//print the score to a file 
	public void printToFile(){
		try{
			PrintWriter fileWriter = new PrintWriter(new FileOutputStream("highscores.txt"),true);
			for (int i=0;i<10;i++){
				if (scores[i] != null){
					fileWriter.println((scores[i].getName()+delim+ scores[i].getPoints()));
				}
				else if(scores[i] == null){ 
					break;
				}	
			}fileWriter.close();
		}
		catch (Exception e){
			System.out.println(e.getMessage());
		}
	}
	
	//print scores to console
	public void toConsole() {
		for (int i=0; i<10; i++){
			if(scores[i]!=null){
				System.out.println("Name: "+scores[i].getName() +"\t"+"Score: "+scores[i].getPoints());
			}
			else{
				break;
			}
		}
	}

//end Database Class	
}
