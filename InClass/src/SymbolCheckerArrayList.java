import java.util.ArrayList;
import java.util.Scanner;

/**
 * @import scanner
 */

/**
 * @author Logan
 *
 */
public class SymbolCheckerArrayList {
	static ArrayList<String> arrayList = new ArrayList<String>(); 
	/*
	 * find match
	 */
	public static boolean findMatch(String checkVal, String closingChar){
		if (arrayList.contains(checkVal)){
			System.out.println(arrayList.get(arrayList.indexOf(checkVal))+" = "+closingChar);
			arrayList.remove(arrayList.indexOf(checkVal)); //if match found remove the matching character
			return true;
		}
		return false;
	}
	/*
	 * switch case support 
	 */
	public static void supportSwitchCase(String openingChar, String closingChar){
		if(findMatch(openingChar,closingChar)==true){
			System.out.println("Match Found In List");
			if(arrayList.isEmpty()==false){ 
				/*
				 * only print if not empty because best case of {}{}{} 
				 * (repeating infinitely) will result in an empty list after each added value
				 * { added then } will be a closing condition, calling the supportSwitchCase 
				 * then it will be true and the list will be empty but then will add a new value when charAt(i+1)
				 * is an opening character and will be added to the list    
				 */
				System.out.println(arrayList);
				return;
			}
		}else{
			System.out.println("Match Not Found For \'" +closingChar+ "\'");
			arrayList.add(closingChar); //if match not found add to list
			System.out.println(arrayList);
		}
	}
	/*
	 * main method 
	 * test input 
	 * 	t<est {case, <t[his< i[s a}{]} t{est< in[put> fr}om th>e us>]er {]}> 
	 */
	public static void main (String[] args){
		boolean again = true;  
		do{
			Scanner kybd = new Scanner(System.in); 
			System.out.println("Enter String to Analyze"); //prompt user
			String userInput = kybd.nextLine(); //take in string
			for(int i=0; i<userInput.length(); i++){ //iterate through the string to find special characters "[,{,(,<"
				System.out.print("Value \'"+ userInput.charAt(i)+ "\'"); 
				if (userInput.charAt(i)=='{' 
						||userInput.charAt(i)=='['
						||userInput.charAt(i)=='('
						||userInput.charAt(i)=='<')
				{ //if they are one of the symbols do work else skip 
					System.out.print(" Is An Opeining Character, Added To List\n");
					arrayList.add(String.valueOf(userInput.charAt(i)));
					System.out.print("The List is: "+arrayList);
				}//end if statement  
				else if (userInput.charAt(i)=='}' 
						||userInput.charAt(i)==']'
						||userInput.charAt(i)==')'
						||userInput.charAt(i)=='>')
				{	
					System.out.print(" Is a Closing Character, Looking For Match\n");
					String originalVal = String.valueOf(userInput.charAt(i));
					switch (userInput.charAt(i)){ //switching on the special character
						/*
						 * for each special character pass in its partner symbol then call the switch case support 
						 * to tell if their is a partner symbol in the list, if so then remove it because it has a match 
						 * if not then add the userInput.charAt(i) value because it does not have a partner
						 */
						case '}': 
							supportSwitchCase("{",originalVal);
							break;
						case ']':
							supportSwitchCase("[",originalVal);
							break;
						case ')':
							supportSwitchCase("(",originalVal);
							break;
						case '>':
							supportSwitchCase("<",originalVal);
							break;
						default:
							System.out.println("default switch ran");
							break;
					}//end switch case 
				}//end else if statement 
				else{
					System.out.print(" Unnecissary character, skipped");
				}
				System.out.println("");	
			}//end for loop 
			//if head is null then all openings had a closing and statement is correct	
			if(arrayList.isEmpty()==true){
				arrayList.toString();
				System.out.println("!True!");	
			}else{ //iff head != null then an opening did not have a closing
				arrayList.toString();
				System.out.println("!False!");
			}
			System.out.println("Go Again? y/n");
			String next = kybd.nextLine();
			if (next.equalsIgnoreCase("y")||next.equalsIgnoreCase("yes")){
				again = true;
			}else{
				System.out.println("Bye");
				again = false; 
			}
		}while (again == true);
	}//end of main
}//end of class
