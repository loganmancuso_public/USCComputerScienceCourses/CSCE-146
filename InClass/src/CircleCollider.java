/**
 * 
 */

/**
 * @author Logan
 *
 */
public class CircleCollider extends Collider{

	private double radius;

	public CircleCollider() {
		super();
		this.radius = 0.0;
	}

	public CircleCollider(double ax, double ay, double aRadii) {
		super(ax, ay);
		this.setRadius(aRadii);
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
	
}
