/**
 * 
 */

/**
 * @author Logan
 *
 */
public class Car {
	private String make, model;
	private int year;
	
	public Car(String make, String model, int year) {
		this.make = make;
		this.model = model;
		this.year = year;
	}
	public Car() {
		this.make = null;
		this.model = null;
		this.year = -1;
	}

	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		if(year >= 0){
			this.year = year;
		}
	}
	
	public String toString(){
		return this.make + " " + this.model + " " + this.year;
	}
	public boolean equals(Car aCar){
		return 
				this.make.equals(aCar.getMake())&&
				this.model.equals(aCar.getModel())&&
				this.year == aCar.getYear();
	}

}
