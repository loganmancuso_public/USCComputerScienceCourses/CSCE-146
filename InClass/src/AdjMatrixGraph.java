
/**
 * 
 */
import java.util.*;

/**
 * @author Logan
 *
 */
public class AdjMatrixGraph {
	private static final int DEF_SIZE = 10;
	private double adjMatrix[][];
	private ArrayList<Integer> markedVert;
	private ArrayList<Integer> visitedVert;

	public AdjMatrixGraph() {
		this.adjMatrix = new double[DEF_SIZE][DEF_SIZE];
		markedVert = new ArrayList<Integer>();
		visitedVert = new ArrayList<Integer>();
	}

	public AdjMatrixGraph(int size) {
		if (size <= 0)
			return;
		this.adjMatrix = new double[size][size];
		markedVert = new ArrayList<Integer>();
		visitedVert = new ArrayList<Integer>();
	}

	public void addEdge(int row, int column, double weight) {
		if (row < 0 || column < 0)
			return;
		adjMatrix[row][column] = weight; // to followed by from
	}

	public void printDFS() {
		markedVert.clear();
		toPrintDFS(0);
	}

	private void toPrintDFS(int index) {
		System.out.println(index); // name is just the 'index'
		markedVert.add(index); // add to marked list
		for (int i = 0; i < adjMatrix.length; i++) {
			if (adjMatrix[index][i] != 0 && markedVert.contains(index) == false) {
				toPrintDFS(index);
			}
		}
		return; // make another call from previous
	}

	public void printBFS() {
		markedVert.clear();
		visitedVert.clear();
		toPrintBFS(0);
	}

	private void toPrintBFS(int index) {
		if (visitedVert.contains(index) == false) {
			System.out.println(index);
			visitedVert.add(index);
		}
		markedVert.add(index);
		// visit neighbors
		for (int i = 0; i < adjMatrix.length; i++) {
			if (adjMatrix[index][i] != 0 && visitedVert.contains(i) == false) {
				System.out.println(i);
				visitedVert.add(i);
			}
		}
		// move to next node
		for (int i = 0; i < adjMatrix.length; i++) {
			if (adjMatrix[index][i] != 0 && markedVert.contains(i) == false) {
				toPrintBFS(i);
			}
		}
		return;
	}

}
