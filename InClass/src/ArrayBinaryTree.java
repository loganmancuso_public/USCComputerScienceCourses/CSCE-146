/**
 * implementation of a binary search tree with an array 
 */

/**
 * @author Logan
 *
 */
public class ArrayBinaryTree <Type extends Comparable<Type>> {
	private static final int DEFAULT_SIZE = 128; //guarantee if balanced use powers of 2
	private Type array[];
	
	public ArrayBinaryTree(){ //default 
		array = (Type[])(new Comparable[DEFAULT_SIZE]);		
	}
	//param constructor 
	/**
	 * @param size
	 */
	public ArrayBinaryTree(int size){ //parameterized 
		if(size>=0){
			array = (Type[])(new Comparable[size]);	
		}return;
	}
	//insert method
	public void insert(Type data){
		if(array[0] == null) //empty tree
			array[0]= data;
		else
			toInsert(0,data); //if root != null set other place 
	}
	//insert helper method
	/**
	 * @param i
	 * @param data
	 * @return
	 */
	private void toInsert(int i, Type data){
		if(array[i]==null)//found, place data 
			array[i]= data;
		else if(data.compareTo(array[i])<0){//left
			if((i*2)+1 < array.length)
				toInsert(i*2+1,data);
		}
		else if(data.compareTo(array[i])>=0){//right
			if((i*2)+2 < array.length)
				toInsert(i*2+2,data);
		}
	}
	//--------------------------- Deletion-------------------------------//
	//delete method
	public void delete(Type value){
		toDelete(value,0);
	}
	//TODO: delete helper
	/**
	 * @param value
	 * @param i
	 * @return
	 */
	private void toDelete(Type value, int i){
		if(i>=array.length || array[i] == null)//find value
			return; //value not found
		if(value.compareTo(array[i])<0 && i*2+1 < array.length)//left
			toDelete(value,i*2+1);
		else if(value.compareTo(array[i])>0 && i*2+2 < array.length)
			toDelete(value,i*2+2);
		else{ //found perform deletion 
			if((i*2+1 < array.length) && (i*2+2 < array.length) && (array[i*2+2]==null)){ //having one or none child 
				moveLeftSubtree(i); //move left sub tree
				return;
			}else if((i*2+2 < array.length) && (array[i]==null)){ 
				moveRightSubtree(i); //moving right sub tree 
				return;
			}else{ //two children 
				//replace curr with min in right sub
				array[i]=findMinInTree(i*2+2);
				//TODO: delete min from tree
				deleteMinFromTree(i*2+2);
				return; 
			}
		}
	}	
	//moving left subtree
	private void moveLeftSubtree(int i){
		if(i>=array.length) //end
			return; 
		boolean isLeft = i%2!=0;
		if(isLeft || i==0){ //left or root
			if(i*2+1 < array.length)
				array[i] = array[i*2+1]; //current with left
			else{
				array[i] = null; //replace to null and return 
				return;
			}
		}else{ //right 
			if((i-2)*2+2 < array.length)
				array[i] = array[(i-1)*2+2];
			else{
				array[i] = null;
				return;
			}
		}
		moveLeftSubtree(i*2+2); //recursive to left 
		moveLeftSubtree(i*2+1); //recursive to right 
	}
	//moving right subtree
	private void moveRightSubtree(int i){
		if(i >= array.length) //end 
			return;
		boolean isLeft = i%2!=0;
		if(!isLeft || i==0){ //right or root
			if(i*2+2 < array.length)
				array[i] = array[i*2+2]; //curr with right
			else{
				array[i] = null;
				return;
			}
		}else
			if((i+1)*2+1 < array.length)
				array[i] = array[(i+1)*2+1]; //replace with siblings left child 
			else{
				array[i] = null;
				return;
			}
		moveRightSubtree(i*2+2);
		moveRightSubtree(i*2+1);
	}
	//find min in the tree
	private Type findMinInTree(int i){
		if(i >= array.length || array[i]==null)
			return null;
		if(i*2+1 >= array.length || array[i*2+1] == null)
			return array[i]; 
		else
			return findMinInTree(i*2+1);
	}
	private void deleteMinFromTree(int i){
		if(i >= array.length)
			return;
		if(i*2+1 < array.length && array[i*2+1]==null){ //found the min 
			if(i*2+2 < array.length && array[i*2+2] != null) //has right child 
				moveRightSubtree(i);
			else{
				array[i] = null; 
				return;
			}
		}
		deleteMinFromTree(i*2+1); 			
	}
	
	//--------------------End Deletion-----------------------// 
	
	//search method
	public boolean search(Type data){
		return toSearch(0,data);
	}
	//search helper method 
	/**
	 * @param i
	 * @param data
	 * @return
	 */
	private boolean toSearch(int i,Type data){
		if(i>=array.length || array[i]==null) //end or null, not found
			return false;
		if(data.compareTo(array[i])==0) //found, no difference in value
			return true;
		else if(data.compareTo(array[i])<0)//left
			return toSearch(i*2+1,data);
		else //if(data.compareTo(array[i])>0)//right
			return toSearch(i*2+2,data);
	}
	/*
	 * traversals
	 * Post Order
	 * Breadth Order
	 * Pre Order 
	 * In Order
	 */
	//post order print method
	public void postOrder(){
		printPostOrder(0);
	}
	private void printPostOrder(int i){
		if(array[i]==null){
			return;
		}
		if((i*2)+1 <= array.length && array[i*2+1] != null){ //visit left
			printPostOrder((i*2)+1);
		}
		if((i*2)+2 <= array.length && array[i*2+1] != null){ //visit right
			printPostOrder((i*2)+2);
		}
		System.out.println(array[i].toString()); //access data
	}
	//breadth order print method
	public void breadthOrder(){
		for(Type val: array){
			if(val != null)
				System.out.println(val.toString());
		}
	}
	
	/*
	 * Main Method 
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Int BST Tester!");
		System.out.println("Creating tree");
		ArrayBinaryTree<Integer> testTree = new ArrayBinaryTree();
		System.out.println("Populating Tree with values");
		int[] valArr = {20,16,28,8,17,25,35,1,9,18,22,33,42};
		for(int i : valArr)
		{
			testTree.insert(i);
		}
//		System.out.println("Testing insertion");
		System.out.println("post order");
		testTree.postOrder();
//		System.out.println("Delete 7 ");
//		testTree.delete(7);
//		System.out.println("Testing breadth order traversal");
//		testTree.breadthOrder();
		
	}
}
