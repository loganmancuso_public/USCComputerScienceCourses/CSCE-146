import java.util.Random;

/**
 * 
 */

/**
 * @author Logan
 *
 */
public class AdjMatrixTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Matrix Size of 7");
		AdjMatrixGraph AdjGraph = new AdjMatrixGraph(7);
		Random r = new Random();
		// adding edges
		AdjGraph.addEdge(0, 1, r.nextInt(4));
		AdjGraph.addEdge(0, 3, r.nextInt(4));
		AdjGraph.addEdge(1, 3, r.nextInt(4));
		AdjGraph.addEdge(3, 2, r.nextInt(4));
		AdjGraph.addEdge(2, 0, r.nextInt(4));
		AdjGraph.addEdge(3, 4, r.nextInt(4));
		AdjGraph.addEdge(2, 4, r.nextInt(4));
		AdjGraph.addEdge(4, 5, r.nextInt(4));
		AdjGraph.addEdge(2, 5, r.nextInt(4));
		AdjGraph.addEdge(4, 6, r.nextInt(4));

		System.out.println("Testing Depth First Search");
		AdjGraph.printDFS();
		System.out.println("Testing Breadth First Search");
		AdjGraph.printBFS();
		System.out.println("\nTesting Depth First Search For All");
		AdjGraph.printDFSForAll();
		System.out.println("\nTesting Breadth First Search For All");
		AdjGraph.printBFSForAll();

	}

}
