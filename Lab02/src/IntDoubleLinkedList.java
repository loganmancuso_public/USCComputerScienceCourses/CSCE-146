/**
 * This prgm will create a list with double links one pointing 
 * to next the next pointing to current. 
 */

/**
 * @author Logan
 *
 */

public class IntDoubleLinkedList {

	private class ListNode{ //internal class
		
		//instance variables 
		private int data;
		private ListNode prevLink;
		private ListNode nextLink; //just link in single link list
		//defualt 
		public ListNode(){
			prevLink = null; //not pointing to any place yet 
			nextLink = null;
			data = 0; //no data stored yet
		}
		
		//parameterized 
		public ListNode(int aData, ListNode prevLinkVal, ListNode nextLinkVal){
			nextLink = nextLinkVal;
			prevLink = prevLinkVal;
			data = aData;
		}
	}//end of private class ListNode
	
	//instance variables of ListNode 
	//declare positions  
	private ListNode head; //first instance
	private ListNode curr; 
	// private ListNode prev; //one before current 
	
	//default 
	public IntDoubleLinkedList(){
		head = null;
		curr = null; 
	//	prev = null; //dont need in double linked because links allow to go back
	}
	//parameterized not needed 
	
	
	/*
	 *  other methods
	 */
	
	
	//go to the next node
	public void goToNext(){
		if (curr!=null){
			curr.prevLink = curr;
			curr = curr.nextLink;
		}
		else{
			System.out.print("Error list empty\nSystem exit");
			System.exit(0);
		}
	}
	
	//go to prev node
	public void goToPrev(){
		if(curr.prevLink != null){
			curr = curr.prevLink;
			curr.prevLink = curr.nextLink;
		}
	}
	
	//get data at curr
	public int getDataAtCurrent(){
		int result = 0;
		if(curr != null){
			result = curr.data;
			System.out.println(result);
		}
		else{
			System.out.print("No node pointing\nSystem exit");
			System.exit(0);
		}return result;
	}
	
	public void addNodeToStart(int aData){
		head = new ListNode (aData, head, curr);
		if ((curr == head.nextLink)&& (curr != null)){
			curr.prevLink = head;
		}
	}
	//inserting data to list
	public void insert(int aData){ //pass in value of aData
		ListNode newNode = new ListNode();
		newNode.data = aData;
		if(head == null){
			head = newNode;
			curr = head;
			return;
		}
		ListNode position = head;
		ListNode prevPos = null;
		while (position != null){
			prevPos = position;
			position = position.nextLink; 
		}
		position = newNode;
		prevPos.nextLink = position;
	}
	
	//set new data at curr
	public void setDataAtCurrent(int aData){
		if(curr != null){
			curr.data = aData; //overwrite curr.data with new data as inputed by user
		}
		else if (head == null){
			this.addNodeToStart(aData);
		}
		else{
			System.out.print("No node pointing\nSystem exit");
			System.exit(0);
		}
	}
	
	//add node after curr
	public void insertNodeAfterCurrent(int aData){
		ListNode newNode = new ListNode(); //newNode is instance of new ListNode() method
		newNode.data = aData;
		//conditions for setting new data
		if (curr != null){
			newNode.nextLink = curr.nextLink;
			curr.nextLink = newNode;
		}
		else if(head != null){
			System.out.print("Error, not intinialized\nSystem exit Line68");
			System.exit(0);
		}
		else{
			System.out.print("List is empty\nSystem exit");
			System.exit(0);
		}
	}
	
	//reset iteration
	public void resetIteration(){
		curr = head;
		curr.prevLink = null;
	}

	//delete the curr node
	public void deleteCurrentNode(){
		if ((curr != null)&&(curr.prevLink != null)){ //other than head
			curr.prevLink.nextLink = curr.nextLink;
			curr.nextLink.prevLink = curr.prevLink;
			curr = curr.nextLink;
			curr.data = 0;
		}
		else if((curr != null)&&(curr.prevLink == null)){ //curr node = head
			head = curr.nextLink;
			curr.data = 0; //delete data value prevent leak
			curr = head;
		}
		else{
			this.resetIteration();
		}
	}
	
	//show the data in the list
	public void showList(){
		ListNode position = head;
		System.out.print("\n");
		while(position != null){
			System.out.print(position.data+" ----> ");
			position = position.nextLink;
		}
		System.out.print("X\n\n");
	}
	
	//if value in list boolean return
	public boolean inList(int here){
		return find(here)!=null;
	}
	
	private ListNode find(int here){
		boolean found = false; //not found yet
		ListNode position = head; //start at beginning
		while((position != null)&& !found){
			int theData = position.data;
			if (theData == here){
				found = true; //if found
			}
			else{
				position = position.nextLink;
			}
		}return position;
	}
	
	
}//end class IntDoubleLinkedList








