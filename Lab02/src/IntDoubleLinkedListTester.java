import java.util.Scanner;

public class IntDoubleLinkedListTester{
	
	
	public static void main(String[] args){
		IntDoubleLinkedList list = new IntDoubleLinkedList();
		Scanner kybd = new Scanner(System.in);
		boolean continueOn = true;
		do{
			System.out.println("Test Linked List"
					+ "\n1 add"
					+ "\n2 remove"
					+ "\n3 insert"
					+ "\n4 display"
					+ "\n5 find numb in list"
					+ "\n6 edit data at point"
					+ "\n7 add after current"
					+ "\n8 display data at position"
					+ "\n9 exit");
			
			int choice = kybd.nextInt();
			switch (choice){
			case 1: //add
				System.out.println("enter number of points to add");
				int numb = kybd.nextInt();
				for (int i=0; i<numb; i++){
					System.out.println("(enter a number)");
					int aData = kybd.nextInt();
					list.insert(aData);
				}
				list.showList();
				break;
			case 2: //remove
				System.out.println("delete current node");
				list.deleteCurrentNode(); //call to delete method
				list.showList();
				break;
			case 3: //insert
				System.out.println("Enter data to insert after current");
				int aData = kybd.nextInt();
				list.insertNodeAfterCurrent(aData); //call to insert method
				list.showList();
				break;
			case 4: //display
				list.showList(); //display list
				break;
			case 5: //find 
				System.out.println("Look for numb in list"
						+ "\nEnter a number");
				int here = kybd.nextInt();
				if (list.inList(here)==true){
					System.out.println("Found in list"); //if value is true display 
				}
				else{
					System.out.println("Not found in List"); //if not display 
				}
				list.showList();
				break;
			case 6: //edit at point 
				System.out.println("edit data at a point in list"
						+ "\nposition (space) data to add");
				int pos = kybd.nextInt()-1;
				aData = kybd.nextInt();
				for(int i=0; i<pos; i++){ //move through list 
					list.goToNext();
				}
				list.setDataAtCurrent(aData);
				list.showList();
				break;
			case 7: //add after curr
				System.out.println("add data to list"
						+ "\nenter a number");
				aData = kybd.nextInt();
				list.insertNodeAfterCurrent(aData);
				list.showList();
				break;
			case 8: //display at point
				System.out.println("Move to position and display data: enter pos");
				pos = kybd.nextInt()-1;
				list.resetIteration();
				for (int i=0; i<pos; i++){
					list.goToNext();
				}
				list.getDataAtCurrent();
				break;
			case 9:
				System.out.println("bye");
				continueOn = false;
				break;
			default:
				System.out.println("error, invalid");
				break;
			}//end switch
		}while (continueOn == true);
	}
	
}