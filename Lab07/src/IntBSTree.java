/**
 * 
 */

/**
 * @author Logan
 *
 */
public class IntBSTree{
	private class Node{
		private int data;
		private Node leftChild;
		private Node rightChild;
		public Node(int aData){ //default 
			this.data = aData;
			this.leftChild = null;
			this.rightChild = null;
		}
	}
	private Node root;
	public int depth = 0; 
	public IntBSTree(){
		root = null;
	}
	/*
	 * other methods 
	 */
	//insert method 
	public void insert(int data){
		//JJ-Code this
		Node nodeToAdd = new Node(data);
		if(root == null){//empty tree
			root = nodeToAdd;
		}else{
			toInsert(root,data);
		}
	}
	/**
	 * @param aNode
	 * @param data
	 * @return aNode
	 */
	private Node toInsert(Node aNode, int data) { //insert helper method
		//Auto-generated method stub
		if(aNode == null){ //if found 
			Node nodeToAdd = new Node(data);
			aNode = nodeToAdd;
		}else if((data-aNode.data)<0){ //go left 
			aNode.leftChild = toInsert(aNode.leftChild,data); //Recursively call
		}else if((data-aNode.data)>=0){ //go right
			aNode.rightChild = toInsert(aNode.rightChild,data); //Recursively call
		}return aNode; 
	}
	//print in order method 
	public void printInorder(){
		//JJ-Code this
		print(root);
	}
	/**
	 * @param aNode 
	 */
	private void print(Node aNode){ //print helper method
		//Auto-generated method stub
		if(aNode == null){ //if no node return 
			return;
		}if(aNode.leftChild!=null){
			print(aNode.leftChild);
		}
		System.out.println(aNode.data); //print to console once moved under node
		if(aNode.rightChild!=null){
			print(aNode.rightChild);
		}
		return;
	}
	//TODO:get depth method 
	public int getDepth(int value){
		//JJ-Code this
		return depth(root,value);
	}
	/**
	 * @param aNode
	 * @param value
	 */
	private int depth(Node aNode, int value){ //calculating depth helper method
		if(aNode==null){
			return -1; 
		}else if(aNode.data-value == 0){ //if found 
			return depth;
		}else if(aNode.data - value > 0){ //go left 
			depth ++;
			return depth(aNode.leftChild,value);
		}else{ //go right
			depth++;
			return depth(aNode.rightChild,value);
		}
	}
} //end class


