import java.io.File;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author Logan
 *
 */
public class SheepHeap<Type> {

	public static final int DEF_SIZE = 10; // default size of array
	private Sheep heap[], sheep;
	private int size;

	public SheepHeap() {
		this.heap = new Sheep[DEF_SIZE];
		this.size = 0;
		this.sheep = null;
	}

	public SheepHeap(int size) {
		if (size > 0) {
			this.heap = new Sheep[size]; // change to user desired size
			this.size = 0;
			this.sheep = null;
		} else {
			return;
		}
	}

	public void fileIn(String fileName) {
		try {
			Scanner fileScanner = new Scanner(new File(fileName));
			while (fileScanner.hasNextLine()) {
				String fileLine = fileScanner.nextLine();
				String split[] = fileLine.split("\t");
				Sheep sheep = new Sheep();
				sheep.setName(split[0]);
				sheep.setWeight(Double.parseDouble(split[1]));
				this.addSheep(sheep);
			}
			fileScanner.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	// insert
	private void addSheep(Sheep sheep) {
		if (size >= heap.length) {
			return;
		}
		heap[size] = sheep;
		climbUp();
		size++;
	}

	private void climbUp() {
		int index = this.size;
		while (index > 0) {
			int parent = index % 2 != 0 ? (index - 1) / 2 : (index - 2) / 2;
			if (parent >= 0 && heap[index].getWeight() - heap[parent].getWeight() < 0) {
				// swap
				Sheep tmp = heap[parent];
				heap[parent] = heap[index];
				heap[index] = tmp;
			} else {
				break;
			}
			index = parent; // move index
		}
	}

	public Sheep peek() {
		if (heap == null)
			return null;
		return heap[0];
	}

	// delete
	public Sheep removeSheep() {
		Sheep retVal = peek();
		if (size > 0) {
			heap[0] = heap[size - 1];
			heap[size - 1] = null; // for null value
			size--;
			climbDown();
		}
		return retVal;
	}

	private void climbDown() {
		int index = 0;
		while (index * 2 + 1 < size) { // do not change this does not change for
										// min or max heap
			int smallIndex = index * 2 + 1;
			if (index * 2 + 2 < size && heap[index * 2 + 1].getWeight() - heap[index * 2 + 2].getWeight() > 0) {
				smallIndex = index * 2 + 2;
			}
			if (heap[index].getWeight() - heap[smallIndex].getWeight() > 0) {
				Sheep tmp = heap[index];
				heap[index] = heap[smallIndex];
				heap[smallIndex] = tmp;
			} else {
				break;
			}
			index = smallIndex;
		}
	}

	// print breadth
	public void sheepRollCall() {
		for (Sheep sheep : heap) {
			if (sheep != null) // doesn't print non initialized "0" or null
								// "-999"
				System.out.println(sheep.getName() + " " + sheep.getWeight());
			else {
				break;
			}
		}
	}

	// heapsort
	public void sheepHeapSort() {
		SheepHeap temp = new SheepHeap(heap.length);
		Sheep deepCopyHeap[] = heap.clone(); // deep copy the heap
		for (int i = 0; i < size; i++) {
			temp.addSheep(deepCopyHeap[i]); // populate
		}
		for (int i = size; i >= 0; i--) {
			if (temp.peek() != null) { // stop from printing null values
				print(temp.removeSheep());
			}
		}
		System.out.print("\n");
	}

	private void print(Sheep sheep) {
		System.out.println(sheep.getName() + "\t" + sheep.getWeight());
	}
}
